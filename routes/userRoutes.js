const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();
  router.get('/', (req, res) => {
    res.send(UserService.getAll());
  })
  router.post('/', createUserValid, (req, res) => {
    const result = UserService.create(req.body);
    console.log(result);
    res.status(200).send(result);
  })
  router.get('/:id', (req, res) => {
    const result = UserService.search({id: req.params.id})
    if (result) {
      res.send(result);
    } else {
      res.status(404).send({ error: true, message: 'User not found' })
    }
  })
  router.put('/:id',updateUserValid ,(req, res) => {
    const result = UserService.update(req.params.id, req.body)
    if (result) {
      res.send(result);
    } else {
      res.status(404).send({ error: true, message: 'User not found' })
    }
  })
  router.delete('/:id', (req, res) => {
    const result = UserService.delete(req.params.id)
    if (result) {
      res.send({ success: true });
    } else{
      res.status(404).send({ error: true, message: 'User not found' })
    }
  })


module.exports = router;