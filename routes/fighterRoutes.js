const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

  router.get('/', (req, res) => {
    res.send(FighterService.getAll());
  })
  router.post('/', createFighterValid, (req, res) => {
    const result = FighterService.create(req.body);
    console.log(result);
    res.status(200).send(result);
  })
  router.get('/:id', (req, res) => {
    const result = FighterService.search({id: req.params.id})
    if (result) {
      res.send(result);
    } else {
      res.status(404).send({ error: true, message: 'Fighter not found' })
    }
  })
  router.put('/:id', updateFighterValid, (req, res) => {
    const result = FighterService.update(req.params.id, req.body)
    if (result){
      res.send(result);
    } else {
      res.status(404).send({ error: true, message: 'Fighter not found' })
    }
  })
  router.delete('/:id', (req, res) => {
    const result = FighterService.delete(req.params.id)
    if (result) {
      res.send({ success: true });
    } else{
      res.status(404).send({ error: true, message: 'Fighter not found' })
    }
  })

module.exports = router;