const powerValidation = (power) => {
  if (!power || power < 0 || power > 100) {
    return { isCorrect: false, msg: 'Power must be [1-99]' };
  }
  return { isCorrect: true };
};

module.exports = powerValidation;