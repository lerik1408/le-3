const passwordValidation = (pass) => {
  if (!pass || pass.length < 3) {
    return { isCorrect: false, msg: 'Password must have 3 characters' };
  }
  return { isCorrect: true };
};

module.exports = passwordValidation;