const emailValidation = require('./email.validation');
const passwordValidation = require('./password.validation');
const phoneValidation = require('./phone.validation');
const powerValidation = require('./power.validation');
const requiredValidation = require('./required.validation');

module.exports = {
  emailValidation,
  passwordValidation,
  phoneValidation,
  powerValidation,
  requiredValidation
}