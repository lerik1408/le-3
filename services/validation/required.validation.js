const requiredValidation = (value, field) => {
  if (!value) {
    return { isCorrect: false, msg: `${field} is required` };
  }
  return { isCorrect: true };
};

module.exports = requiredValidation;