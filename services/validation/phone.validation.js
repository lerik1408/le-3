const phoneValidation = (phone) => {
  const re = /^((8|\+380)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{9}$/;
  const isCorrect = re.test(phone);
  return { isCorrect, msg: isCorrect ? null : 'Invaild phone' };
};

module.exports = phoneValidation;