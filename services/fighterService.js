const { FighterRepository } = require('../repositories/fighterRepository');
const { fighter } = require('../models/fighter')
class FighterService {
    // TODO: Implement methods to work with fighters
    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
    getAll() {
        const fighters = FighterRepository.getAll();
        return fighters;
    }
    create(body) {
        const keys = Object.keys(fighter)
        keys.splice(keys.indexOf('id'), 1);
        let data = {}
        keys.forEach((element) => {
            data[element] = body[element]
        });
        return FighterRepository.create(data);
        
    }
    update(id, body) {
        const keys = Object.keys(fighter)
        keys.splice(keys.indexOf('id'), 1);
        let data = {}
        keys.forEach((element) => {
            data[element] = body[element]
        });
        const item = this.search({ id });
        if (item) {
            return FighterRepository.update(id, data)
        }
        return false
    }
    delete(id) {
        const item = this.search({ id });
        if (item) {
            FighterRepository.delete(id)
            return true
        }
        return null
    }
}

module.exports = new FighterService();