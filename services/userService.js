const { UserRepository } = require('../repositories/userRepository');
const { user } = require('../models/user');

class UserService {

    // TODO: Implement methods to work with user

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
    getAll() {
        const users = UserRepository.getAll();
        return users;
    }
    create(body) {
        const keys = Object.keys(user)
        keys.splice(keys.indexOf('id'), 1);
        let data = {}
        keys.forEach((element) => {
            data[element] = body[element]
        });
        return UserRepository.create(data);
        
    }
    update(id, body) {
        const keys = Object.keys(user)
        keys.splice(keys.indexOf('id'), 1);
        let data = {}
        keys.forEach((element) => {
            data[element] = body[element]
        });
        const item = this.search({ id });
        if (item) {
            return UserRepository.update(id, data)
        }
        return false
    }
    delete(id) {
        const item = this.search({ id });
        if (item) {
            UserRepository.delete(id)
            return true
        }
        return null
    }
}

module.exports = new UserService();