const {
    emailValidation,
    phoneValidation,
    passwordValidation,
    requiredValidation
} = require('../services/validation/index');

const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    const email = emailValidation(req.body.email);
    email.isCorrect || res.status(400).send({
        error: true,
        message: email.msg
    });
    const firstName = requiredValidation(req.body.firstName, 'firstName');
    firstName.isCorrect || res.status(400).send({
        error: true,
        message: firstName.msg
    });
    const lastName = requiredValidation(req.body.lastName, 'lastName');
    lastName.isCorrect || res.status(400).send({
        error: true,
        message: lastName.msg
    });
    const phoneNumber = phoneValidation(req.body.phoneNumber);
    phoneNumber.isCorrect || res.status(400).send({
        error: true,
        message: phoneNumber.msg
    });
    const password = passwordValidation(req.body.password);
    password.isCorrect || res.status(400).send({
        error: true,
        message: password.msg,
    });

    next();
}

const updateUserValid = createUserValid;

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;