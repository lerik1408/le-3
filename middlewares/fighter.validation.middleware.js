const { fighter } = require('../models/fighter');
const {
    emailValidation,
    phoneValidation,
    passwordValidation,
    requiredValidation,
    powerValidation,
} = require('../services/validation/index');
const createFighterValid = (req, res, next) => {
    const name = requiredValidation(req.body.name, 'name');
    name.isCorrect || res.status(400).send({
        error: true,
        message: name.msg
    });
    const health = requiredValidation(req.body.health, 'firstName');
    health.isCorrect || res.status(400).send({
        error: true,
        message: health.msg
    });
    const power = powerValidation(req.body.power);
    power.isCorrect || res.status(400).send({
        error: true,
        message: power.msg
    });
    const defense = requiredValidation(req.body.defense, 'defense');
    defense.isCorrect || res.status(400).send({
        error: true,
        message: defense.msg
    });

    next();
}

const updateFighterValid = createFighterValid;

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;